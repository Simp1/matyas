choice = input('''

+ Plus
- Mínus
* Násobení
/ Dělení
''')

num_1 = int(input('První číslo: '))
num_2 = int(input('Druhé čislo: '))

if choice == '+':
    print('{} + {} = '.format(num_1, num_2))
    print(num_1 + num_2)

elif choice == '-':
    print('{} - {} = '.format(num_1, num_2))
    print(num_1 - num_2)

elif choice == '*':
    print('{} * {} = '.format(num_1, num_2))
    print(num_1 * num_2)

elif choice == '/':
    print('{} / {} = '.format(num_1, num_2))
    print(num_1 / num_2)
