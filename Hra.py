jmeno = ""
ryby=0
energie=100
peníze=10000
nalada=100
životy=100
dmg = 100
def start():
    print("         **                   **       *******                      **          ** **")
    print("        /**                  /**      /**////**                    //          /**//*")
    print("        /**  ******    ***** /**  **  /**    /**  ******   *******  **  *****  /** /   ******")
    print("        /** //////**  **///**/** **   /**    /** //////** //**///**/** **///** /**    **////")
    print("        /**  ******* /**  // /****    /**    /**  *******  /**  /**/**/******* /**   //*****")
    print("    **  /** **////** /**   **/**/**   /**    **  **////**  /**  /**/**/**////  /**    /////**")
    print("   //***** //********//***** /**//**  /*******  //******** ***  /**/**//****** ***    ******")
    print("    /////   ////////  /////  //  //   ///////    //////// ///   // //  ////// ///    //////")
    print("----------------------------------------------------------")
    print("Ahoj,jdeme hrát")
    print("----------------------------------------------------------")
    print("Hra je o chlapcovi jmenem Jack který prochází světem a zabíji mostra a na cestách se shledá se starou známou")
    print("----------------------------------------------------------")
    print("Která se jmenuje Daniels a zamiluje se do ní ")
    print("----------------------------------------------------------")
    print("Hlavní hrdina je trošku hloupí klučík s Odipovským komplexem")
    print("----------------------------------------------------------")
    print("Cíl: tvuj cil je dostat se na konec a porazit Burešovce")
    print("----------------------------------------------------------")
    print("To je vše co ti řeknu dal objevuj sam")
    print("----------------------------------------------------------")
    print("Hele před tim než začnem tak tě musím varovat nikdy ale nikdy neposlouchej pruvodce je to sarkastickej blbec")
    print("----------------------------------------------------------")
    print("co se ti bude snažit hazet klacky pod nohy")
    print("----------------------------------------------------------")
    print("přeji hodně zábavy")
    print("----------------------------------------------------------")
    jmeno = ("Jack")
    print(jmeno +",co chceš nyní dělat?")
    print("L = jít do hospody")
    print("R = jít k rybníku")
    print("M = jít na most")
    volba = input("Tvoje volba:")
    if (volba == "L"):
        hospoda()
    elif(volba == "R"):
        rybník()
    elif(volba == "M"):
        most()
    else:
        print("špatná odpoved")
        start()

def hospoda():
    global energie
    energie = energie - 5
    print("----------------------------------------------------------")
    print("přišel jsi do hospody")
    print("----------------------------------------------------------")
    print(jmeno + ", co chceš dělat?")
    print("----------------------------------------------------------")
    print("c = koupit si pivo")
    print("v = koupit si ženu")
    print("r = jít dál")
    volba = input("Tvoje volba:")
    if (volba == "c"):
        koupitsipivo()
    elif(volba == "v"):
        koupitsiženu()
    elif(volba == "r"):
        jítdál()
    else:
        print("špatná odpoved")
        hospoda()

def koupitsiženu():
    global energie
    global nalada
    global peníze
    peníze = peníze -100
    energie = energie -10
    nalada = nalada +20
    print("užil sis noc s mladou dívkou a ted je čas se vydat na další dobrodružství!")
    hospoda()

def koupitsipivo():
    global energie
    global peníze
    global nalada
    energie = energie + 4
    peníze = peníze - 15
    nalada = nalada + 10
    print("Dal sis jdno chladný točený a ted mužeš jit dal")
    hospoda()

def jítdál():
    print("----------------------------------------------------------")
    print("kam ted ?")
    print("----------------------------------------------------------")
    print("L = jít do hospody")
    print("R = jít k rybníku")
    print("M = jít na most")
    volba = input("Tvoje volba:")
    if (volba == "L"):
        hospoda()
    elif(volba == "R"):
        rybník()
    elif(volba == "M"):
        most()
    else:
        print("špatná odpoved")
        jítdál()

def rybník():
    global energie
    energie = energie -3
    print("----------------------------------------------------------")
    print("přišel jsi k rybníku")
    print("----------------------------------------------------------")
    print(jmeno + ", co chceš dělat?")
    print("----------------------------------------------------------")
    print("c = jít na molo")
    print("v = jít zpět")
    print("r = chytat ryby")
    volba = input("Tvoje volba:")
    if (volba == "c"):
        jitnamolo()
    elif(volba == "v"):
        bejl()
    elif(volba == "r"):
        chytatryby()
    else:
        print("špatná odpoved")
        rybník()

def bejl():
    start()

def chytatryby():
    print("podařilo se ti chytit rybu")
    print("----------------------------------------------------------")
    print("achievement unlocked")
    print("----------------------------------------------------------")
    print("'########:'####::'######::'##::::'##:'########:'########::'##::::'##::::'###::::'##::: ##:")
    print(" ##.....::. ##::'##... ##: ##:::: ##: ##.....:: ##.... ##: ###::'###:::'## ##::: ###:: ##:")
    print(" ##:::::::: ##:: ##:::..:: ##:::: ##: ##::::::: ##:::: ##: ####'####::'##:. ##:: ####: ##:")
    print(" ######:::: ##::. ######:: #########: ######::: ########:: ## ### ##:'##:::. ##: ## ## ##:")
    print(" ##...::::: ##:::..... ##: ##.... ##: ##...:::: ##.. ##::: ##. #: ##: #########: ##. ####:")
    print(" ##:::::::: ##::'##::: ##: ##:::: ##: ##::::::: ##::. ##:: ##:.:: ##: ##.... ##: ##:. ###:")
    print(" ##:::::::'####:. ######:: ##:::: ##: ########: ##:::. ##: ##:::: ##: ##:::: ##: ##::. ##:")
    print("..::::::::....:::......:::..:::::..::........::..:::::..::..:::::..::..:::::..::..::::..::")
    print("----------------------------------------------------------")
    rybník()

def jitnamolo():
    global energie
    energie = energie - 1
    print("----------------------------------------------------------")
    print("přišel jsi na molo a vidíš tam nymfu")
    print("----------------------------------------------------------")
    print(jmeno + ", co chceš dělat?")
    print("----------------------------------------------------------")
    print("c = zautočit na nymfu")
    print("v = popovídat si s nymfou")
    print("r = otočit se a odejít")
    volba = input("Tvoje volba:")
    if (volba == "c"):
        utok()
    elif(volba == "v"):
        povidatsi()
    elif(volba == "r"):
        odejít()
    else:
        print("špatná odpoved")
        jitnamolo()

def utok():
    print("vytasil jsi meč a máchl snim po nymfě")
    print("----------------------------------------------------------")
    print("nebyl to moc dobrý nápad neumší přece bojovat šašku :))))")
    print("----------------------------------------------------------")
    print("no prostě tě čapla za nohu a utopila tě")
    print("----------------------------------------------------------")
    print("Jelikož jsem dnes štědrý dam ti ještě jednu šanci :DD")
    jitnamolo()

def povidatsi():
    print("----------------------------------------------------------")
    print("Nymfa na tebe svudně kouká")
    print("----------------------------------------------------------")
    print("Hodini si povídate a odpočivate ")
    print("----------------------------------------------------------")
    print("Ale nbídne ti potešení")
    print("----------------------------------------------------------")
    print("A ty s radostí přijmeš a ona te utopí")
    print("----------------------------------------------------------")
    print("Hele chlape :D takhle to nepujde já chapu že máš rad ženy")
    print("Ale takhle fakt ne co si jako čekal dávam ti poslední šanci :D")
    jitnamolo()

def odejít():
    rybník()

def most():
    global energie
    energie = energie -2
    print("----------------------------------------------------------")
    print("přišel jsi k mostu")
    print(jmeno + ", co chceš dělat?")
    print("c = odpočivat")
    print("v = pokračovat k rybníku")
    print("f = jít dál po mostě")
    volba = input("Tvoje volba:")
    if (volba == "f"):
        jitdalpomoste()
    elif(volba == "c"):
        odpočivat()
    elif(volba == "v"):
        rybos()
    else:
        print("špatná odpoved")
        most()

def odpočivat():
    global energie
    energie = energie + 5
    print("odpočíval jsi a získal plno energie ted přestan blbnout a jdi něco dělat")
    most()

def rybos():
    rybník()

def jitdalpomoste():
    global energie
    energie = energie - 3
    print("jsi na konci mostu kam dál?")
    print("c == jit doprava")
    print("v == jit doleva")
    volba = input("Tvoje volba:")
    if (volba == "c"):
        jitdoprava()
    elif(volba == "v"):
        jitdoleva()
    else:
        print("špatná odpoved")
        jitdalpomoste()

def jitdoleva():
    global energie
    energie = energie -5
    print("----------------------------------------------------------")
    print("došel jsi do vesnice jaká to parádička")
    print("----------------------------------------------------------")
    print("tyjo chlape tyady je to fakt dobrý co budem dělat")
    print("----------------------------------------------------------")
    print("co ona tady není hospoda tak to je fakt bída")
    print("----------------------------------------------------------")
    print("ale jsou tady hezké holky tak aspon něco")
    print("----------------------------------------------------------")
    print("co ted ? :))))")
    print("----------------------------------------------------------")
    print("dle mého nazoru ta třetí je fakt dobrá")
    print("----------------------------------------------------------")
    print("c == promluvit si s dívkou1")
    print("v == promluvit si s dívkou2")
    print("f == promluvit si s dívkou3")
    volba = input("Tvoje volba")
    if (volba == "c"):
        divka1()
    elif(volba == "v"):
        divka2()
    elif(volba == "f"):
        divka3()
    else:
        print("špatná odpoved")
        jitdoleva()

def divka1():
    print("No jak tak koukám tak s ženama to moc neumíš radši pojd dělat něco jiného :))))")
    takdal()

def divka2():
    print("No jak tak koukám tak s ženama to moc neumíš radši pojd dělat něco jiného :))))")
    takdal()

def divka3():
    print("No jak tak koukám tak s ženama to moc neumíš radši pojd dělat něco jiného :))))")
    takdal()

def takdal():
    print("----------------------------------------------------------")
    print("No dobře promin trochu jsem si z tebe dělal srandu :)))")
    print("----------------------------------------------------------")
    print("Tak ted vážně :)))")
    print("----------------------------------------------------------")
    print("c == jít na radnici ")
    print("v == jít do banky ")
    print("f == projit se po městě")
    volba = input("Tvoje volba")
    if (volba == "c"):
        radnice()
    elif(volba == "v"):
        banka()
    elif(volba == "f"):
        procházka()
    else:
        print("špatná odpoved")
        takdal()

def procházka():
    global energie
    energie = energie -12
    print(" hmm došel jsi na náměstí kám se vydáme ted ")
    print("----------------------------------------------------------")
    print("v == jít na bojiště")
    print("f == jít do kovárny")
    print("g == jít na tancovačku")
    print("c == odejit z vesnice")
    volba = input("Tvoje volba")
    if(volba == "v"):
        boj()
    elif(volba == "f"):
        kovárna()
    elif(volba == "g"):
        tanec()
    elif(volba == "c"):
        putovat()
    else:
        print("špatná odpoved")
        procházka()

def tanec():
    print("Došel jsi na vesnickou tancovačku plnou dobrého jidla a pití")
    print("----------------------------------------------------------")
    print("Uživaš si den a pomalu se setmívá a je na čese vydat se dal")
    procházka()

def kovárna():
    print("Došel jsi do Kovárny")
    print("Co by sis přal")
    print("c == novou zbroj")
    print("v == nový meč")
    print("f == zpet")
    volba = input("tvoje volba")
    if(volba == "c"):
        meč()
    elif(volba == "v"):
        zbroj()
    elif(volba == "f"):
        procházka()
    else:
        print("špatná odpoved")
        kovárna()

def meč():
    print("Kovař ti ukoval nový meč")
    print("          #                     #  ")
    print("         #  ### ### ### ### ###  #  ")
    print("### ###  #                        #  ")
    print("         #  ### ### ### ### ###  #  ")
    print("          #                     #  ")
    kovárna()

def zbroj():
    print("Kovář ti ukoval novou zbroj")
    kovárna()

def boj():
    global energie
    energie = energie -3
    print("došel jsi na bojiště")
    print("c == naučit se s mečem")
    print("v == dát si sváču")
    print("f == zpět")
    volba = input("Tvoje Volba")
    if(volba == "c"):
        trenál()
    elif(volba == "v"):
        chála()
    elif(volba == "f"):
        procházka()
    else:
        print("špatná odpoved")
        boj()

def chála():
    global energie
    energie = energie +13
    print("Dal sis dobrotu která ti přidala energii ")
    boj()

def trenál():
    global energie
    energie = energie -25
    print("Trenoval jsi opravdu hodně a naučil jsi se s mečem ")
    boj()

def putovat():
    print("odcházíš z vesnice a putuješ dál kam tě cesty zavedou to ještě nevíš ale brzo se nekam dostaneš ")
    print("----------------------------------------------------------")
    print("3h pozdeji")
    print("----------------------------------------------------------")
    print("procházíš lesem a nikde nic ale najendou uslyšíš křičet dívku o pomoc")
    print("----------------------------------------------------------")
    print("tak běžíš za hlasem")
    print("----------------------------------------------------------")
    print(" a najednou uvidíš dívku a dva muže ")
    print("----------------------------------------------------------")
    print("tak tasíš meč a jdeš bojovat")
    print("----------------------------------------------------------")
    print("doufám že to umíš pokud ne :))) tak seš pěkně nahranej")
    zachrana()

def zachrana():
    print("vytasil si na nepřítele meč a setnul jim hlavu ")
    print("----------------------------------------------------------")
    print("Rozvázal jsi ženu a začal jsis s ni povídat ")
    print("----------------------------------------------------------")
    print("Zjistil jsi že se jmenuje Daniel's ")
    print("----------------------------------------------------------")
    print("Až dojdete k málé chlupě u lesa")
    print("----------------------------------------------------------")
    print("Vejdete dovnitř a uvnitř je monstrum takzvaný Opluštilovec")
    print("----------------------------------------------------------")
    print("tasíš meč a jdeš do boje")
    print("----------------------------------------------------------")
    print("c == utnout mu hlavu")
    print("v == probodnout ")
    volba = input("tvoje volba")
    if(volba == "c"):
        hlava()
    elif(volba == "v"):
        bodnout()
    else:
        print("špatná odpoved")
        zachrana()

def bodnout():
    print("Bodnout jsi mečem a s monstrem byl konec")
    print("----------------------------------------------------------")
    print("Prozkoumal jsi chalupu a rozhodnul jsi se zde s dívkou přenocovat")
    print("----------------------------------------------------------")
    print("Ležíte v posteli a chystáte se spát")
    print("----------------------------------------------------------")
    print("c == navodit atmošku")
    print("v == jít spát")
    volba = input("Tvoje volba")
    if (volba == "c"):
        atmoška()
    elif (volba == "v"):
        spát()
    else:
        print("špatná odpoved")
        bodnout()

def hlava():
    print("Máchnul jsi mečem a s monstrem byl konec")
    print("----------------------------------------------------------")
    print("Prozkoumal jsi chalupu a rozhodnul jsi se zde s dívkou přenocovat")
    print("----------------------------------------------------------")
    print("Ležíte v posteli a chystáte se spát")
    print("----------------------------------------------------------")
    print("c == navodit atmošku")
    print("v == jít spát")
    volba = input("Tvoje volba")
    if(volba == "c"):
        atmoška()
    elif(volba == "v"):
        spát()
    else:
        print("špatná odpoved")
        hlava()

def atmoška():
    print("Navodil si atmošku a s Daniel's sis užil večer :))))")
    print("----------------------------------------------------------")
    print("Doufám že sis to užil možná to bylo naposled")
    cesta()

def cesta():
    print("----------------------------------------------------------")
    print("Po skvělém večeru jste se vydali na cestu ")
    print("----------------------------------------------------------")
    print("A došeli jste do obrovského města ")
    print("----------------------------------------------------------")
    print("c == jít k výveskové tabuli")
    print("v == jít do hotelu")
    print("f == jít do loděnice")
    print("d == jít do domu s kočkoholkama")
    print("g == jít do alchimistické dílny")
    volba = input("Tvoje Volba:")
    if(volba == "c"):
        tabule()
    elif(volba == "v"):
        hotel()
    elif(volba == "f"):
        lod()
    elif(volba == "d"):
        čiči()
    elif(volba == "g"):
        alchimista()
    else:
        ("neumíš pismenka ty kkt nebo co ?")
        cesta()

def alchimista():
    print("vytej v alchimistické dílně")
    print("----------------------------------------------------------")
    print("zde seženeš vše potřebné")
    print("----------------------------------------------------------")
    print("tak vybírej")
    print("----------------------------------------------------------")
    print("c == lektvar léčení")
    print("v == lektvar síly")
    print("f == lektvar na posílení erekce")
    print("d == lektvar proti chlamidi")
    print("g == lektvar proti HIV")
    print("o == lektvar na výdrž")
    print("R == zpět")
    volba = input("Co si dáš:")
    if(volba == "c"):
        lečení()
    elif(volba == "v"):
        síla()
    elif (volba == "f"):
        erekce()
    elif (volba == "d"):
        chlam()
    elif (volba == "g"):
        HIV()
    elif (volba == "o"):
        výdrž()
    elif (volba == "R"):
        cesta()
    else:
        print(">:((( vidim že ti škola dost chyběla když nejsi schopnej zmáčknout na písmenko co vidíš")
        alchimista()

def léčení():
    global peníze
    global energie
    peníze = peníze -500
    energie = energie +20
    print("Vyborně dal jsi si lektvar a ted se mužeš vydat dal nebo chceš ještě njaky")
    alchimista()

def síla():
    global peníze
    global energie
    peníze = peníze - 500
    energie = energie + 20
    print("Vyborně dal jsi si lektvar a ted se mužeš vydat dal nebo chceš ještě njaky")
    alchimista()

def erekce():
    global peníze
    global energie
    peníze = peníze - 500
    energie = energie + 20
    print("Vyborně dal jsi si lektvar a ted máš pořádnýho tvrďáka mužeš se vydat dal nebo chceš ještě njaky")
    alchimista()

def chlam():
    global peníze
    global energie
    peníze = peníze - 500
    energie = energie + 20
    print("Vyborně dal jsi si  lektvar a ted se mužeš vydat dal nebo chceš ještě njaky")
    alchimista()

def HIV():
    global peníze
    global energie
    peníze = peníze - 500
    energie = energie + 20
    print("Vyborně dal jsi si  lektvar a ted se mužeš vydat dal nebo chceš ještě njaky")
    alchimista()

def výdž():
    global peníze
    global energie
    peníze = peníze - 500
    energie = energie + 20
    print("Vyborně dal jsi si  lektvar a ted se mužeš vydat dal nebo chceš ještě njaky")
    alchimista()

def hotel():
    print("přišel jsi do hotelu a vypadá to že je tu dnes dost rušno")
    print("----------------------------------------------------------")
    print("c == koupit si pokoj")
    print("v == jit do wellness")
    print("d == nabídnout si dívku")
    volba = input("Tvoje volba:")
    if(volba == "c"):
        pokoj()
    elif(volba == "v"):
        well()
    elif(volba == "d"):
        devka()
    else:
        print("špatná volba")
        hotel()

def pokoj():
    global energie
    energie = energie +10
    print("koupil sis pokoj a prospal jsi se")
    print("----------------------------------------------------------")
    print("ted se muzes vydat dal")
    hotel()

def well():
    global energie
    energie = energie +500
    print("užil sis velnes odpočinul sis a starali se o tebe krásné ženy")
    print("----------------------------------------------------------")
    print("Ale nenech se rozptylovat a pokračuj dal v ceste ")
    hotel()

def devka():
    global energie
    global nalada
    energie = energie - 30
    nalada = nalada +10
    print("užil sis s dívkou a ted mužeš jít dal")
    print("----------------------------------------------------------")
    print("chapu že se ti neche :D")
    print("----------------------------------------------------------")
    print("ale utíkej")
    hotel()

def lod():
    print("Přišel jsi do loděnice")
    print("----------------------------------------------------------")
    print("ale nic tu není")
    print("----------------------------------------------------------")
    print("asi by jsi se mel vratit zpět")
    print("----------------------------------------------------------")
    print("Přepadli tě loumpové tak je musíš je zprášit")
    fajt()

def fajt():
    global energie
    energie = energie - 10
    print(" Aby jsi je porazil musíš utoky poslat v dsobrém pořadí")
    print("----------------------------------------------------------")
    print("c == utok zleva ")
    print("v == utok zprava")
    print("d == bodnout")
    volba = input("tvoje volba:")
    if(volba == "c"):
        jedna()
    else:
        print("jsi mrtvej")
        lod()

def jedna():
    print("vyborně jednoho jsi zkolil")
    print("----------------------------------------------------------")
    print("Tak pokračuj na další dva")
    print("----------------------------------------------------------")
    print("v == utok zprava")
    print("d == bodnout")
    volba = input("tvoje volba:")
    if (volba == "d"):
        dva()
    else:
        print("jsi mrtvej")
        lod()

def dva():
    print("Vyborně zabil jsi i druhého ted doraž posledního")
    print("----------------------------------------------------------")
    print("v == utok zprava")
    volba = input("tvoje volba:")
    if (volba == "v"):
        tri()
    else:
        print("jsi mrtvej")
        lod()

def tri():
    print("Zabil jsi je paráda")
    print("----------------------------------------------------------")
    print("ted zpět do města")
    cesta()

def čiči():
    print(":O tak ty máš dost místo normálních holek si tady ujíždíš na kočko holkách")
    print("----------------------------------------------------------")
    print("No ty máš dost tady máš něco co ti bude připomínat že si uchyl")
    print("----------------------------------------------------------")
    print("achievement unlocked")
    print("----------------------------------------------------------")
    print("                  __                  ___  ")
    print("                 /\ \                /\_ \   ")
    print("  __  __     ___ \ \ \___    __  __  \//\ \   ")
    print(" /\ \/\ \   /'___ \ \  _ `\ /\ \/\ \   \ \ \   ")
    print(" \ \ \_\ \ /\ \__/ \  \ \ \ \ \ \_\ \   \_\ \_   ")
    print("  \ \____/ \ \____\ \ \_\ \ _\ /`____ \ /\____\ ")
    print("   \/___/   \/____/  \/_/\/_/`/___/> \/   ____/ ")
    print("                                /\___/      ")
    print("                                \/__/    ")
    cesta()

def tabule():
    print("----------------------------------------------------------")
    print("vidíš zde jediný ukol")
    print("----------------------------------------------------------")
    print("Na skolení monstra které delá už dlouho problém městu")
    print("----------------------------------------------------------")
    print("chceš tento ukol příjmout")
    print("A == Ano")
    print("N == Ne")
    volba = input("Tvoje volba:")
    if(volba == "A"):
        Ano()
    elif(volba == "N"):
        OFC()
    else:
        print("vybyrej z toho co vidíš")
        tabule()

def OFC():
    Ano()

def Ano():
    print("----------------------------------------------------------")
    print("Výborně vydal jsi se do bažiny u města hledat monstrum")
    print("----------------------------------------------------------")
    print("NAJEDNOU SE Z BAŽINY VYNOŘÍ OBŘÍ MONSTRUM")
    print("----------------------------------------------------------")
    print("Hend od pohledu jsi vedel ze to je Burešovec Bažinný ")
    print("----------------------------------------------------------")
    print("V životě jsi nikdy tak velkého Burešovce neviděl")
    print("----------------------------------------------------------")
    print("Tohohle musíš zdolat")
    print("----------------------------------------------------------")
    print("tak se pust do boje na co čekáš")
    print("----------------------------------------------------------")
    print("zmáčkni R pro start")
    volba = input("Tvoje volba")
    if(volba == "R"):
        BOSS()
    else:
        print("Já fakt nevím nauč se písmena ještě jednou to posereš a jdeš na začátek")
        Ano()

def BOSS():
    print("aby jsi Burešovce zabil musíš utoky poslat ve správnem pořadí")
    print("----------------------------------------------------------")
    print("Když to pokazíš zemřeš a vracíš se na poslední zachyntý bod")
    print("----------------------------------------------------------")
    print("H == utok na hlavu")
    print("N == utok na nohu")
    print("P == utok na penis")
    volba = input("Tvoje Volba:")
    if(volba == "N"):
        one()
    else:
        print("posral jsi to upaluj zpět")
        BOSS()

def one():
    print("výborně seknul jsi ho do nohy ")
    print("----------------------------------------------------------")
    print("ale to nestačí musíš pokračovat")
    print("----------------------------------------------------------")
    print("H == utok na hlavu")
    print("P == utok na penis")
    volba = input("Tvoje volba:")
    if(volba == "H"):
        two()
    else:
        print("chyba!! tak znova")
        BOSS()

def two():
    print("paráda rozseknul jsi mu pusu")
    print("----------------------------------------------------------")
    print("už nemuže ale jeho pravá síla je v jeho penisu")
    print("----------------------------------------------------------")
    print("P == utok na penis")
    volba = input("Tvoje volba")
    if(volba == "P"):
        konec()
    else:
        print(" tak to ne vsak uz jsi skoro u konce ")
        BOSS()

def spát():
    print("vole nebuď gay :D a jdi do ní")
    atmoška()

def banka():
    print("----------------------------------------------------------")
    print("co budem delat?")
    print("----------------------------------------------------------")
    print("c == pujčit si penize")
    print("v == uložit si penize")
    print("f == jit zpět")
    volba = input("Tvoje volba:")
    if(volba == "c"):
        pujčit()
    elif(volba == "v"):
        uložit()
    elif(volba == "f"):
        back()
    else:
        print("špatná odpoved")
        banka()

def pujčit():
    global peníze
    peníze = peníze +500
    banka()

def uložit():
    global peníze
    peníze = peníze -100
    banka()

def back():
    takdal()

def radnice():
    print("----------------------------------------------------------")
    print("došel si na radnici")
    print("----------------------------------------------------------")
    print("co dal ?")
    print("----------------------------------------------------------")
    print("c == vařidit si cestovní prukaz")
    print("v == jit pryč")
    volba = input("tvoje volba")
    if(volba == "v"):
        vratit()
    elif(volba == "c"):
        prukaz()
    else:
        print("špatná odpoved")
        radnice()

def vratit():
    takdal()

def prukaz():
    print("----------------------------------------------------------")
    print("dobrý den přišel jsem si vyřidit cestovní prukaz")
    print("----------------------------------------------------------")
    print("Paní:Dobře bude to 150 zlatých")
    print("----------------------------------------------------------")
    print("c == zaplatit")
    print("v == zkusit svuj šarm")
    print("f == odejit")
    volba = input("tak co?")
    if(volba == "c"):
        pladba()
    elif(volba == "v"):
        flirt()
    elif(volba == "f"):
        takdal()
    else:
        print("špatná odpoved")
        prukaz()

def jitdoprava():
    global energie
    energie = energie -5
    print("přišel jsi do lesa")
    print("----------------------------------------------------------")
    print("co ted ?")
    print("----------------------------------------------------------")
    print("c == jít prozkoumat les")
    print("v == vrátit se")
    volba = input("Tvoje volba:")
    if (volba == "c"):
        jitzkoumat()
    elif (volba == "v"):
        vratitse()
    else:
        print("špatná odpoved")
        jitdoprava()

def jitzkoumat():
    global energie
    energie = -10
    print("----------------------------------------------------------")
    print("po dlouhém zkoumani a běhaní po lese si našel velké hovno")
    print("co si čekal že tu bude je to přece les :))))")
    print("----------------------------------------------------------")
    jitdalpomoste()

def vratitse():
    global energie
    energie = energie -5
    print("----------------------------------------------------------")
    print("jako srab jsi se vydal zpět jaké zklamání :)))")
    print("----------------------------------------------------------")
    jitdalpomoste()


def pladba():
    global peníze
    peníze = peníze -150
    print("Jaký to slušný chlapec ja bych ji bodnul a utekl :D hehe ")
    takdal()

def flirt():
    print("----------------------------------------------------------")
    print("Abych nastinil situaci")
    print("----------------------------------------------------------")
    print("Přišel jsi na paní mrknul a začal s ní flirtovat")
    print("----------------------------------------------------------")
    print("A ona te polila horkým čajem XDDD ty si fakt jelito")
    print("----------------------------------------------------------")
    prukaz()

def konec():
    print("VÝBORNĚ PORAZIL JSI BUREŠOVCE JAKO TROFEJ JSI ZÍSKAL JEHO PENIS")
    print("----------------------------------------------------------")
    print("ZDE PŘIBĚH KONČÍ ALE NEZOUFEJ ZA 50E SI MUŽEŠ DOKOUPIT DLC")
    print("----------------------------------------------------------")
    print("ASI TĚ ZAJÍMÁ JAK TO VLASTNĚ VŠE SKONČILO")
    print("----------------------------------------------------------")
    print("POTOM CO JSI ZABIL BUREŠOVCE JSI SE VRATIL DO MĚSTA")
    print("----------------------------------------------------------")
    print("KDE SE SLAVILO A NECHALA SE TI POSTAVIT SOCHA")
    print("----------------------------------------------------------")
    print("ALE POTÉ JSI ZJISTIL ZE MÁŠ POHLAVNÍ NEMOC NA KTEROU ZA 20LET ZEMŘEŠ")
    print("----------------------------------------------------------")
    print("A VÍŠ PROČ? JELIKOŽ JSI NADRŽENÉ HOVADO CO SE VYSPALO S  Daniel's ")
    print("----------------------------------------------------------")
    print("A TAKY JSI ZJISTIL ŽE JE TO TVOJE MATKA A TY BANDITY KTERÉ JSI ZABIL BYLI TVUJ OTEC S BRATREM")
    print("----------------------------------------------------------")
    print("PROSTĚ JSI NECHUTNÝ HOVADO. UŽIVEJ ŽIVOT ")
    print("----------------------------------------------------------")
    print("'##:::'##::'#######::'##::: ##:'########::'######::")
    print(" ##::'##::'##.... ##: ###:: ##: ##.....::'##... ##:")
    print(" ##:'##::: ##:::: ##: ####: ##: ##::::::: ##:::..::")
    print(" #####:::: ##:::: ##: ## ## ##: ######::: ##:::::::")
    print(" ##. ##::: ##:::: ##: ##. ####: ##...:::: ##:::::::")
    print(" ##:. ##:: ##:::: ##: ##:. ###: ##::::::: ##::: ##:")
    print(" ##::. ##:. #######:: ##::. ##: ########:. ######::")
    print("..::::..:::.......:::..::::..::........:::......:::")
    print("----------------------------------------------------------")
    print("E == pokračování")
    volba = input("Zmačkni:")
    if(volba == "E"):
        nigga()
    else:
        konec()

def nigga():
    print("----------------------------------------------------------")
    print("Herci: Burešovec = Martin Bureš")
    print("----------------------------------------------------------")
    print("Opluštilovec = Jakub Opluštil")
    print("----------------------------------------------------------")
    print("Hlavní Hrdina Jack = Filip Sláma")
    print("----------------------------------------------------------")
    print("Daniel's = Michaela Kovářová")
    print("----------------------------------------------------------")
    print("alchimista = Harley Davidson")
    print("----------------------------------------------------------")
    print("nymfa = Nika")
    print("----------------------------------------------------------")
start()
